const model = require("../models/index");

module.exports = {
    createUser: async function (userDetails) {
        try {
            userDetails.isActive = true;
            let createUserStatus = await model.createNewUser(userDetails);
            return {
                status: true,
                statusText: "User Created Successfully",
                statusCode: "200",
                error: null
            }
        }
        catch (err) {
            throw err;
        }
    }
};