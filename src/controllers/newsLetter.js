const model = require("../models/index");
const util = require("../utility/index");
const addToQueue = require("../utility/addToQueue");
const sender = require("../utility/sendNewsLetter");

module.exports = {
    triggerNewsLetter: async function (csvData) {
        try {
            let newsLetterDataJson = await util.convertCsvToJson(csvData);
            if (typeof newsLetterDataJson !== "string") {
                newsLetterDataJson[0].map(async data => {
                    if (!data["email address"]) {
                        delete data
                    } else {
                        let createUserStatus = await model.checkForUser({ "email": data["email address"] });
                        data.firstname = '';
                        data.lastname = '';
                        if (Object.keys(createUserStatus).length > 0) {
                            data.firstname = createUserStatus.firstname;
                            data.lastname = createUserStatus.lastname;
                        }
                        let addMessageToQueue = await addToQueue(data, "sendNewsLetter");
                    }
                });
                let sendMessage = sender();
                return {
                    status: true,
                    statusText: "News Letters Added to Queue",
                    statusCode: "200",
                    error: null
                }
            }
            else {
                throw "911"
            }
        }
        catch (err) {
            throw err;
        }
    }
};