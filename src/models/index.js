const mongoose = require("mongoose");
require('dotenv').config({ path: __dirname + '/../.env' });
const url = process.env["MONGO_HOST"] + ":" + process.env["MONGO_PORT"] + "/" + process.env["MONGO_DB"]
const userDataSchema = mongoose.Schema({

	firstname: {
		type: String,
		required: [true]
	},
	lastname: {
		type: String,
		required: [true]
	},
	email: {
		type: String,
		required: [true]
	},
	age: {
		type: Number,
		required: [true]
	},
	isActive: {
		type: Boolean,
		required: [true]
	}
});
const logSchema = mongoose.Schema({

	date: {
		type: Date,
		required: [true]
	},
	email: {
		type: String,
		required: [true]
	},
	newsletter_name: {
		type: String,
		required: [true]
	}
});
module.exports = {
	connectDb: async function () {
		try {
			return mongoose.connect(url, { useUnifiedTopology: true, useNewUrlParser: true });
		} catch (err) {
			throw err;
		}
	},
	createNewUser: async function (data) {
		try {
			const dbo = await this.connectDb();
			let storeUserData = mongoose.model('userData', userDataSchema, 'userData');
			let newUser = new storeUserData(data)
			let response = await newUser.save();
			return response;
		} catch (err) {
			throw err;
		}
	},
	addNewLog: async function (data) {
		try {
			const dbo = await this.connectDb();
			let addLog = mongoose.model('logs', logSchema, 'logs');
			let log = new addLog(data)
			let response = await log.save();
			return response;
		} catch (err) {
			throw err;
		}
	},
	checkForUser: async function (query) {
		try {
			const dbo = await this.connectDb();
			let searchUser = mongoose.model('userData', userDataSchema, 'userData');
			const response = await searchUser.findOne(query).lean();
			return response;
		} catch (err) {
			throw err;
		}
	}


}