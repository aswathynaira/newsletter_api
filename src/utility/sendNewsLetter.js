
const amqplib = require('amqplib');
require('dotenv').config({ path: __dirname + '/../.env' });
const amqglibUrl = process.env["AMQBLIB_URL"]
const model = require("../models/index");
const nodemailer = require("nodemailer");
const addToQueue = require("./addToQueue");

module.exports = function () {
    let identifier = 'sendNewsLetter';
    let open = amqplib.connect(amqglibUrl);
    open.then(function (conn) {
        return conn.createChannel();
    }).then(function (ch) {
        return ch.assertQueue(identifier).then(function (ok) {
            return ch.consume(identifier, function (msg) {
                if (msg !== null) {
                    let incomingMessage = JSON.parse(msg.content);
                    let transporter = nodemailer.createTransport({
                        host: process.env["MAIL_HOST"],
                        port: process.env["MAIL_PORT"],
                        secure: false, // true for 465, false for other ports
                        auth: {
                            user: process.env["MAIL_USERNAME"],
                            pass: process.env["MAIL_PASSWORD"],
                        },
                    });
                    let mailOptions = {
                        from: process.env["MAIL_FROM"],
                        to: incomingMessage["email address"],
                        subject: "News Letter",
                        html: `<p>Hi ${incomingMessage["firstname"]} ${incomingMessage["lastname"]}</p></br><p> ${incomingMessage["newsletter content"]} </p>`, // html body
                    };
                    transporter.sendMail(mailOptions, function (err, info) {
                        if (err) {
                            let addMessageToPendingQueue = addToQueue(incomingMessage, "parking-lot-queue");
                        } else {
                            let log = { "email": logData["email address"], "newsletter_name": logData["newsletter name"], "date": new Date() }
                            let addLog = model.addNewLog(log);
                            return incomingMessage;
                        }
                    });
                }
            });
        });
    }).catch(console.warn);
}