
const amqplib = require('amqplib');
require('dotenv').config({ path: __dirname + '/../.env' });
const amqglibUrl = process.env["AMQBLIB_URL"]

module.exports = function (data, identifier) {
    identifier = identifier ? identifier : 'sendNewsLetter';
    let open = amqplib.connect(amqglibUrl);

    open.then(function (conn) {
        return conn.createChannel();
    }).then(function (ch) {
        return ch.assertQueue(identifier).then(function (ok) {
            return ch.sendToQueue(identifier, Buffer.from(JSON.stringify(data)));
        });
    }).catch(console.warn);
}