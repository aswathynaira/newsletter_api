const csvtojson = require("csvtojson");

module.exports = {
	formatErrors: function (err) {
		try {
			if (typeof err === "string") {
				let statusText = "";
				switch (err) {
					case "910":
						statusText = "Uploaded file type should be csv";
						break;
					case "911":
						statusText = "The file should contain headers email address, newsletter content, newsletter name";
						break;
					case "912":
						statusText = "Upload a csv file with key `file`";
						break;
					default:
						statusText = "Internal server error occured";
						break;
				}
				return {
					errorObj: { status: false, statusCode: 400, statusText, data: null, error: null }
				};
			}
			if (!err.hasOwnProperty('array')) {
				let statusText = "Internal server error occured";
				return {
					errorObj: { status: false, statusCode: "500", statusText, data: null, error: null }
				};
			}
			let errors = err.array();
			let errorMsgs = {};

			for (let i in errors) {
				if (errors[i].msg && !errors[i].nestedErrors) {
					errorMsgs[errors[i].param] = errors[i].msg;
				}
				if (errors[i].nestedErrors) {
					for (let j in errors[i].nestedErrors) {
						errorMsgs[errors[i].nestedErrors[j].param] = errors[i].nestedErrors[j].msg;
					}
				}
			}
			return {
				errorObj: {
					status: false,
					statusCode: "400",
					statusText: "Validation errors in the request",
					data: null,
					error: {
						validationErrors: errorMsgs
					}
				}
			};
		} catch (err) {
			let statusText = "Internal server error occured";
			return {
				errorObj: { status: false, statusCode: "500", statusText, data: null, error: null }
			};
		}
	},
	convertCsvToJson: function (csvData) {
		try {
			let data = csvData.toString('utf8');
			let csvArr = data
			csvArr = csvArr.split("\r");
			csvArr = csvArr[0].split(",");
			if (csvArr.length < 3 || csvArr.indexOf("email address") === -1 || csvArr.indexOf("newsletter content") === -1 || csvArr.indexOf("newsletter name") === -1) {
				throw ("911")
			}
			return csvtojson().fromString(data).then(json => {
				return [json]
			})
		} catch (err) {
			return err;
		}
	}

}