const express = require('express');
const fileUpload = require('express-fileupload');
const indexRouter = require('./routes/index');
const app = express();

app.use(fileUpload());
app.use(express.urlencoded({ extended: false }));
app.use(express.json());
app.use('/', indexRouter);

app.use(function (req, res, next) {
	return res.status(404).send({
		status: false,
		statusCode: "400",
		statusText: "Requested method not found",
		data: null,
		error: null
	});
});

app.use((err, req, res, next) => {
	if (err instanceof SyntaxError && err.status === 400 && 'body' in err) {
		return res.status(400).send({
			status: false,
			statusCode: "400",
			statusText: "Invalid JSON/Bad request",
			data: null,
			error: null
		});
	}
});

const port = 3000;
app.set('port', port);
app.listen(port);