const express = require('express');
const router = express.Router();
const { check, validationResult } = require('express-validator/check');
const util = require("../utility/index");
const addUserController = require("../controllers/createUser");
const newsLetterController = require("../controllers/newsLetter");

router.post('/addUser', [
	check('firstname').isString().withMessage('Please provide a valid firstname').not().isEmpty().withMessage("Please provide a firstname").trim(),
	check('lastname').isString().withMessage('Please provide a valid lastname').not().isEmpty().withMessage("Please provide a lastname").trim(),
	check('email').isEmail().withMessage('Please provide a valid email').not().isEmpty().withMessage("Please provide an email").trim(),
	check('age').isInt().withMessage('Please provide a valid age').not().isEmpty().withMessage("Please provide an age").trim(),
], async (req, res) => {
	try {
		validationResult(req).throw();
		let response = await addUserController.createUser(req.body);
		res.send(response);
	} catch (err) {
		let { code, errorObj } = util.formatErrors(err);
		res.status(400).send(errorObj);
	}
});

router.post('/triggerNewsletter', async (req, res) => {
	try {
		validationResult(req).throw();
		if (!req.files	|| (req.files && typeof req.files.file === 'undefined')){
			throw "912";
		}
		let uploadFileDetails = req.files.file;
		if (uploadFileDetails.mimetype !== "text/csv") {
			throw "910";
		}
		let response = await newsLetterController.triggerNewsLetter(uploadFileDetails.data);
		res.send(response);
	} catch (err) {
		let { code, errorObj } = util.formatErrors(err);
		res.status(400).send(errorObj);
	}
});

module.exports = router;