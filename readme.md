# NEWSLETTER API

This service performs various operations like adding new user to the system and create a queue to add the email jobs for sending news letters to the users and consume the data from the queue to send emails. 

## Deployment Steps

To run this service, first we need to install the dependencies and need to set configuration values.

```bash
npm install
touch .env
# Edit .env file with the following content with any editor
nano .env
node app.js
```

Contents of `.env` file

```dosini
MONGO_PORT=27017 
MONGO_HOST=mongodb://localhost
MONGO_DB=user_newsletter
AMQBLIB_URL=amqps://xbhzwqsm:pNTV7rzxX3RXBVXNieqdz_6VaW36p87t@snake.rmq2.cloudamqp.com/xbhzwqsm
MAIL_HOST=
MAIL_PORT=
MAIL_USERNAME=
MAIL_PASSWORD=
MAIL_FROM=
```
# Open APIs

## Create a new User

### Request

`POST http://localhost:3000/addUser`

### Request sample

    {
    "firstname":"John",
    "lastname":"Smith",
    "email":"johnsmith@gmail.com",
    "age":"40"
    }


## Send Newsletter
This api will accept a csv file which has the user email address , newsletter content and newsletter name.Sample csv is added in the repository under location newsletter_api\src\sample\sample.csv

API will create a queue to add the email jobs for sending news letters to the users and consume the data from the queue to send emails.

### Request

`POST http://localhost:3000/triggerNewsletter`

### Request sample

    {"file":sample.csv}

[![Run in Postman](https://run.pstmn.io/button.svg)](https://app.getpostman.com/run-collection/4104f464cf4175c8b195?action=collection%2Fimport)